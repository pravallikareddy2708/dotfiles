#!/bin/sh
if [ ! -f "${XDG_STATE_HOME:-$HOME/.local/state}/weasley/dotstow" ]; then
    kitty -e weasley startup
fi
