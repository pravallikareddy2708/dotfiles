# dotfiles

> My dotfiles - enjoy!

## Install

### Install Single Packages

```sh
make <SOME_PACKAGE>
```

### Install All Packages

```sh
make
```
